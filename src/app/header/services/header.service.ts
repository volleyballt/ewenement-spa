import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class HeaderService {

  constructor() { }

  observer: Observer<string[]>;
  menu: Observable<string[]> = new Observable((observer) => this.observer = observer);
  slider: Observable<string[]> = new Observable((observer) => this.observer = observer);

  sendMenuItems() {
    return this.menuItems;
  }
  sendSliderItems () {
    return this.sliderItems;
  }

  menuItems = [
    {
      name: 'projektowanie wnętrz',
      route: ''
    },
    {
      name: 'wizualizacje 3D',
      route: 'http://ewenement.net/wizualizacje/'
    }
  ];

  sliderItems = [
    'pasja i inspiracja',
    'kreowanie przestrzeni',
    'dobry design'
  ];
}
