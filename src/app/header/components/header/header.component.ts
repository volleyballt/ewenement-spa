import {Component, OnInit} from '@angular/core';
import {HeaderService} from '../../services/header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private headerService: HeaderService) {
  }

  menuItems;
  sliderItems: string[];

  ngOnInit() {
    this.menuItems = this.headerService.sendMenuItems();
    this.sliderItems = this.headerService.sendSliderItems();
    this.slider();
  }

  slider() {
    document.addEventListener("DOMContentLoaded", function (event) {
      const headerBox = document.getElementsByClassName("box-header-slider-item");
      const slidesArray = Array.prototype.slice.call(headerBox);
      const elemHeight = slidesArray[0].offsetHeight;

      for (let i = 0; i < slidesArray.length; i++) {
        slidesArray[i].style.top = (elemHeight * i - 40) + "px";
      }

      let counter = 0;

      setInterval(() => {
        for (let i = 0; i < slidesArray.length; i++) {
          slidesArray[i].style.display = "inline";
          let currentTop = slidesArray[i].offsetTop;
          slidesArray[i].style.top = (currentTop + elemHeight) + "px";
        }
        slidesArray[slidesArray.length - 1 - counter % slidesArray.length].style.display = "none";
        slidesArray[slidesArray.length - 1 - counter % slidesArray.length].style.top = (-elemHeight) + "px";
        counter++;
      }, 2000);
    });
  }

}
