import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObjectiveComponent} from './components/objective/objective.component';
import {DataTransferService} from './services/data-transfer.service';
import { OfferComponent } from './components/offer/offer.component';
import { StageComponent } from './components/stage/stage.component';
import {HtmlPipe} from './pipes/html.pipe';
// import { DocumentationComponent } from './components/documentation/documentation.component';
// import { ReasonComponent } from './components/reason/reason.component';
// import { DoYouKnowComponent } from './components/do-you-know/do-you-know.component';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import { ReferencesComponent } from './components/references/references.component';
import { SliderComponent } from './components/slider/slider.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule} from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  declarations: [
    ObjectiveComponent,
    OfferComponent,
    StageComponent,
    HtmlPipe,
    // DocumentationComponent,
    // ReasonComponent,
    // DoYouKnowComponent,
    AboutMeComponent,
    ContactDetailsComponent,
    ReferencesComponent,
    SliderComponent
  ],
  providers: [ DataTransferService ],
  exports: [
    ObjectiveComponent,
    OfferComponent,
    StageComponent,
    // DocumentationComponent,
    // ReasonComponent,
    // DoYouKnowComponent,
    AboutMeComponent,
    ContactDetailsComponent,
    ReferencesComponent,
    SliderComponent,
    HtmlPipe,
  ]
})
export class PageModule { }
