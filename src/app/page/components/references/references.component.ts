import { Component, OnInit } from '@angular/core';
import { DataTransferService } from '../../services/data-transfer.service';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';


@Component({
  selector: 'app-references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.scss'],
  animations:
    // [
    //   trigger('flyInOut', [
    //     state('in', style({transform: 'translateX(0)'})),
    //     transition('* => *', [
    //       style({transform: 'translateX(-100%)'}),
    //       animate(1000)
    //     ]),
    //     transition('* => *', [
    //       animate(1000, style({transform: 'translateX(500px)'}))
    //     ])
    //   ])
    // ]

    [
      trigger('heroState', [
        state('left', style({
          backgroundColor: '#eee',
          // transform: 'scale(1)',
        transform: 'translateX(100%)'
        })),
        state('active',   style({
          backgroundColor: '#cfd8dc',
          // transform: 'scale(1.1)',
          transform: 'translateX(0)'
        })),
        state('right',   style({
          backgroundColor: '#cfd8dc',
          // transform: 'scale(1.1)',
          transform: 'translateX(-100%)'
        })),
        transition('left => active', animate(1000, style({transform: 'translateX(0)'}))),
        transition('right => active', animate(1000, style({transform: 'translateX(0)'}))),
        transition('active => left', animate(1000, style({transform: 'translateX(-100%)'}))),
        transition('active => right', animate(1000, style({transform: 'translateX(100%)'})))
      ])
    ]

})
export class ReferencesComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }


  toggleState(state) {
    return state === 'active' ? 'inactive' : 'active';
  }


name: string;


  htmlData;

  currentIndex = 0;
  tempLength: number = 0

  setCurrentSlideIndex = function (index) {
    this.currentIndex = index;
  };

  isCurrentSlideIndex = function (index) {
    return this.currentIndex === index;
  };

  prevSlide = function () {
    this.currentIndex = (this.currentIndex < this.htmlData.length - 1) ? ++this.currentIndex : 0;
    this.alterState();
  };
  nextSlide = function () {
    this.currentIndex = (this.currentIndex > 0) ? --this.currentIndex : this.htmlData.length - 1;
    this.alterState();
  };

  alterState() {
    this.tempLength = this.htmlData.length;

    for(let i = 0; i < this.htmlData.length; i++){
      this.htmlData[i].state = 'inactive'
    }
    if(this.currentIndex === 0){
      this.htmlData[this.currentIndex].state = 'active';
      this.htmlData[this.currentIndex + 1].state = 'right';
      this.htmlData[this.tempLength - 1].state = 'left';
    } else if (this.currentIndex === this.tempLength -1){
      this.htmlData[this.currentIndex].state = 'active';
      this.htmlData[0].state = 'right';
      this.htmlData[this.currentIndex - 1].state = 'left';
    } else {
      this.htmlData[this.currentIndex].state = 'active';
      this.htmlData[this.currentIndex + 1].state = 'right';
      this.htmlData[this.currentIndex - 1].state = 'left';
    }
  }



  ngOnInit() {
    this.dataTransferService.referencesEmitter.subscribe(data => this.htmlData = data);
    this.dataTransferService.getData('references')

  }

}
