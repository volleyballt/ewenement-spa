import { Component, OnInit } from '@angular/core';
import { DataTransferService} from '../../services/data-transfer.service';

@Component({
  selector: 'app-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.scss']
})
export class StageComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }

  htmlData: object = {};

  ngOnInit() {
    this.dataTransferService.stageEmitter.subscribe(data => this.htmlData = data)
    this.dataTransferService.getData('stage');
  }

}
