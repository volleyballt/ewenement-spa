import { Component, OnInit } from '@angular/core';
import { DataTransferService} from '../../services/data-transfer.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }

  htmlData: object = {};

  ngOnInit() {
    this.dataTransferService.offerEmitter.subscribe(data => this.htmlData = data)
    this.dataTransferService.getData('offer');
  }

}
