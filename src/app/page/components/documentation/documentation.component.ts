import { Component, OnInit } from '@angular/core';
import { DataTransferService } from '../../services/data-transfer.service';

@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.component.html',
  styleUrls: ['./documentation.component.scss']
})
export class DocumentationComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }

  htmlData: object = {};

  ngOnInit() {
  this.dataTransferService.documentationEmitter.subscribe(data => this.htmlData = data);
  this.dataTransferService.getData('documentation')
  }

}
