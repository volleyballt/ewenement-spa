import { Component, OnInit } from '@angular/core';
import { DataTransferService } from '../../services/data-transfer.service';

@Component({
  selector: 'app-do-you-know',
  templateUrl: './do-you-know.component.html',
  styleUrls: ['./do-you-know.component.scss']
})
export class DoYouKnowComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }

  htmlData: object = {};
  buttonClick(){
    console.log("cos z routerem do kontaktow")
  }


  ngOnInit() {
    this.dataTransferService.doYouKnowEmitter.subscribe(data => this.htmlData = data);
    this.dataTransferService.getData('doYouKnow')
  }

}
