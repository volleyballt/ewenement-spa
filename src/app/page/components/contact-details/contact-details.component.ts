import { Component, OnInit } from '@angular/core';
import { DataTransferService } from '../../services/data-transfer.service';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }

  htmlData: object = {};
  buttonClick(){
    console.log("cos z routerem do kontaktow")
  }


  ngOnInit() {
    this.dataTransferService.contactDetailsEmitter.subscribe(data => this.htmlData = data);
    this.dataTransferService.getData('contactDetails')
  }

}
