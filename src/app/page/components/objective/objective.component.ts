import { Component, OnInit } from '@angular/core';
import { DataTransferService} from '../../services/data-transfer.service';

@Component({
  selector: 'app-objective',
  templateUrl: './objective.component.html',
  styleUrls: ['./objective.component.scss']
})
export class ObjectiveComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }

  htmlData: object = {};

  ngOnInit() {
    this.dataTransferService.objectiveEmitter.subscribe(data => this.htmlData = data)
    this.dataTransferService.getData('objective');
  }

}
