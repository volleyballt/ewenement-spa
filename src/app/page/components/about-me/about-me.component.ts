import { Component, OnInit } from '@angular/core';
import { DataTransferService } from '../../services/data-transfer.service';

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss']
})
export class AboutMeComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }

  htmlData: object = {};



  ngOnInit() {
    this.dataTransferService.aboutMeEmitter.subscribe(data => this.htmlData = data);
    this.dataTransferService.getData('aboutMe')
  }


}
