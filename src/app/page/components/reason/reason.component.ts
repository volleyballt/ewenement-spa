import { Component, OnInit } from '@angular/core';
import { DataTransferService } from '../../services/data-transfer.service';

@Component({
  selector: 'app-reason',
  templateUrl: './reason.component.html',
  styleUrls: ['./reason.component.scss']
})
export class ReasonComponent implements OnInit {

  constructor(private dataTransferService: DataTransferService) { }

  htmlData: object = {};

  ngOnInit() {
    this.dataTransferService.reasonEmitter.subscribe(data => this.htmlData = data);
    this.dataTransferService.getData('reason')
  }

}
