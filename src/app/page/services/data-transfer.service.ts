import {Injectable} from '@angular/core';
import {Observer} from 'rxjs/Observer';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class DataTransferService {

  constructor() {
  }

  emittedData = {
    objective: {
      h1: 'PROJEKT - PO CO ?',
      text: `<span class='capitalize'>poszukiwanie </span><span class='emphasize'>PIĘKNA</span> W OTOCZENIU CZŁOWIEK MA W SWOJEJ NATURZE. <span class='capitalize'>pragnienie</span> <span class='emphasize'>ESTETYKI</span> ISTNIEJE W KAŻDYM Z NAS. <span class='capitalize'>chcemy</span>, ABY NASZE OTOCZENIE BYŁO ŁADNE, <span class='emphasize'>DOPASOWANE</span> DO NASZYCH POTRZEB, <span class='emphasize'>FUNKCJONALNE</span>   I WYGODNE. <span class='capitalize'> architekt </span>I JEGO PROJEKT POMOGĄ CI OSIĄGNĄĆ TĘ <span class='emphasize'>HARMONIĘ</span> WE WŁASNYCH CZTERECH ŚCIANACH. <span class='capitalize'>spełniają </span>TWOJE OCZEKIWANIA, BY PRZESTRZEŃ WOKÓŁ <span class='emphasize'>CIEBIE</span> NABRAŁA CECH TWOJEGO CHARAKTERU I ZACZĘŁA ISTNIEĆ DLA CIEBIE.`
    },
    offer: {
      h1: 'OFERTA',
      h2: 'JAKOŚĆ TO DZISIAJ LUKSUS, A TY MASZ TO W ZASIĘGU RĘKI.',
      text: '<span class=\'capitalize\'>zajmuję</span> się PROJEKTOWANIEM WNĘTRZ PRYWATNYCH: <span class=\'emphasize\'>DOMÓW</span> \n' +
      'I  <span class=\'emphasize\'>MIESZKAŃ</span>. <span class=\'capitalize\'> największą </span> SATYSFAKCJĘ DAJE MI SPEŁNIENIE OCZEKIWAŃ LUDZI ODNOŚNIE ICH WŁASNEGO KAWAŁKA MIEJSCA NA ZIEMI. <span class=\'capitalize\'>troszczę </span>SIĘ O KAŻDY  <span class=\'emphasize\'>DETAL</span> ABY FINALNY PROJEKT BYŁ  <span class=\'emphasize\'>PERFEKCYJNY</span> DLA MNIE \n' +
      'I DLA CIEBIE.  <span class=\'emphasize capitalize\'>kompleksowe </span> PROJEKTOWANIe WNĘTRZ TO OSTATECZNIE ZBIÓR WIELU ELEMENTÓW, KTÓRE POZWOLĄ NA URZECZYWISTNIENIE TWOICH POTRZEB. <span class=\'capitalize\'>tutaj </span> MOŻESZ DOWIEDZIEĆ SIĘ JAK PRZEBIEGA CAŁY  <span class=\'emphasize\'>PROCES PROJEKTOWANIA</span>.'
    },
    stage: [
      {
        h1: 'ETAP 1',
        img: '<i class="fa fa-search"></i>',
        subtext: 'Start...',
        h2: 'HARMONOGRAM',
        p1: 'Rozmowa',
        p2: 'Inwentaryzacja',
        p3: 'Układ funkcjonalny'
      },
      {
        h1: 'ETAP 2',
        img: 'link do img',
        subtext: '...Proces projektowania...',
        h2: 'HARMONOGRAM',
        p1: 'Koncepcja',
        p2: 'Wizualizacje 3D',
        p3: 'Dobór materiałów i mebli'
      },
      {
        h1: 'ETAP 3',
        img: 'link do img',
        subtext: '...Zakończenie',
        h2: 'HARMONOGRAM',
        p1: 'Wizyty w sklepach',
        p2: 'Dokumentacja',
        p3: 'Nadzór'
      }
    ],
    numbers: {
      title: 'MOJA PASJA W LICZBACH',
      subtext: [
        {
          number: 2760,
          text: 'm<sup>2</sup> zaprojektowanych przestrzeni'
        },
        {
          number: 785,
          text: 'godzin spędzonych w sklepach branżowych'
        },
        {
          number: 1950,
          text: 'rozróżnianych barw kolorów'
        },
        {
          // ./../../../assets/icons/infinity.svg
          number: `infinity`,
          text: 'satysfakcja'
        }
      ]
    },
    aboutMe: {
      title: '"EWENEMENT -',
      text: '<span class="center">- wyjątkowe, nietypowe zdarzenie, które wyróżnia się na tle codzienności..."</span></br></br>\n' +
      '\n' +
      'Ukończyłam Wydział Architektury i Urbanistyki na Politechnice Krakowskiej \n' +
      'w 2008 roku. Już na studiach moje największe zainteresowanie wzbudzało właśnie projektowanie wnętrz. Po ukończeniu nauki dalej rozwijałam i realizowałam się \n' +
      'w tym kierunku. Tworzę swoje projekty za każdym razem z wielką pasją \n' +
      'i zaangażowaniem. Dążenie do doskonałości to cecha, która przyświeca mi przy każdym z nich. Dlatego pracę uznaję za zakończoną w momencie, gdy niczego nie można już dodać, ani odjąć, aby było lepiej. To właśnie jest "ewenement"... </br></br>\n' +
      '<span class="center">Zapraszam</span></br></br>\n' +
      '\n' +
      '<span class="center">Ewelina Cieplińska</span>'
    },
    documentation: {
      h1: 'PROJEKT JEST PREZENTOWANY W POSTACI <span class="emphasize">DOKUMENTACJI</span>, KTÓRA ZAWIERA PROFESJONALNE ARCHITEKTONICZNE RYSUNKI:',
      text: ['- RZUTY I WIDOKI ŚCIAN/PRZEKROJE',
        '- FOTOREALISTYCZNE WIZUALIZACJE 3D',
        '- RYSUNKI DETALI W WIĘKSZEJ SKALI',
        '- DOKŁADNE RYSUNKI DLA WYKONAWCÓW'
      ]
    },
    reason: {
      h2: 'DLACZEGO JA?',
      h1: 'PROJEKTY, KTÓRE TWORZĘ SĄ WYJĄTKOWE',
      text: [
        {
          title: 'INNOWACYJNE',
          text: 'To co robię, to poszukiwanie najlepszych rozwiązań pod względem estetycznym i funkcjonalnym \n' +
          'we wnętrzach mieszkalnych. Aby tak było najważniejsza jest znajomość nowych materiałów \n' +
          'i nowoczesnych technologii oraz śledzenie aktualnych trendów. Jednak każdorazowo projekt musi być indywidualnie dopasowany do klienta. Dlatego testuję nowinki i tworzę nowe standardy. Mogę zaoferować Ci innowacyjny projekt \n' +
          'na dobrym poziomie.'
        },
        {
          title: 'DOPASOWANE',
          text: 'Każda osoba czy rodzina jest inna. Profesjonalny projekt jest oparty na bazie Twoich potrzeb \n' +
          'i wykończony dobrym designem. Jest dopasowany, ponieważ troszczę się o preferencje każdego klienta, jego budżet i ramy czasowe. Potrafię projektować różne wnętrza, od klasycznych po nowoczesne, ascetyczne. Najważniejsza jest wiedza i wyczucie artystyczne, a ja jestem po to, aby zaprojektować wszystko, czego potrzebujesz.'
        },
        {
          title: 'PERFEKCYJNE',
          text: 'Projekt jest produktem. Aby był perfekcyjny musi być oparty na wykwalifikowanej wiedzy i najlepszej praktyce. Na koniec musi być opracowany jako dokumentacja zawierająca wszelkie niezbędne rysunki techniczne oraz wizualizacje 3D. Tylko to pozwoli na bezproblemowy przebieg remontu, aby móc finalnie poczuć się komfortowo \n' +
          'w wykończonych i urządzonych własnych czterech kątach.'
        }
      ]
    },
    doYouKnow: {
      text1: 'CZY WIESZ, ŻE CENA PROJEKTU TO NIECAŁE <span class="emphasize" id="chuuuj">2%</span> KOSZTU CAŁEJ INWESTYCJI? JESTEŚ GOTOWY',
      text2: '<span class="emphasize">ZACZĄĆ ROZMOWĘ </span>?',
      button: 'Napisz'
    },
    contactDetails: {
      email: 'wnetrza@ewenement.net',
      phoneNumber: '888-709-803',
      personal: 'ewenement, <span class="span">Ewelina Cieplińska, Kraków</span>'
    },
    references: [
      {
        person: 'Ewa i Krzysztof, Kraków',
        text: 'Z pełną satysfakcją stwierdzamy, że Pani Ewelina w pełni profesjonalny sposób z uwzględnieniem naszych oczekiwań, potrzeb, pomysłów, budżetu dotrzymując harmonogramu wykonała wszelkie umówione prace z pełnym zaangażowaniem. Dzięki jej rekomendacjom i poleceniom mogliśmy w harmonijny, bezstresowy sposób przeprowadzić całą inwestycję począwszy od samej koncepcji przez wybór materiałów i wykonawców, a na odbiorze prac budowlanych kończąc. Niniejszym, gorąco polecamy współpracę z Panią Eweliną Cieplińską, która w naszym oczekiwaniu zaowocuje Państwa zadowoleniem z tego wyboru.',
        state: 'in'
      },
      {
        person: 'Ania i Jacek, Warszawa',
        text: 'Mieliśmy przyjemność współpracować z Panią Eweliną przy tworzeniu wnętrza naszego domu. Krótko pisząc jest to osoba niesamowicie profesjonalna a oprócz tego że ma talent jest niesamowicie pracowita i dokładna, dopieszcza każdy szczegół projektu czym nas uwiodła zanim się na nią zdecydowaliśmy oglądając jej portfolio. Dodatkowo Pani Ewelina jest bardzo życzliwą i taktowną osobą. Nasza współpraca była bardzo owocna, nasze oczekiwania zostały w 100% spełnione i możemy ją polecić każdemu kto w wyborze projektanta jest bardzo surowy, my tacy byliśmy zanim wybraliśmy osobę która stworzy wnętrze domu naszych marzeń :) Raz jeszcze polecamy!',
        state: 'inactive'
      },
      {
        person: 'Kasia i Michał, Katowice',
        text: 'Oczywiście, najłatwiej byłoby napisać, że ma świetny gust…Byłoby to być może wystarczające gdyby na samym projekcie zabawa się kończyła. Nie, nie, nie - tu przygoda się zaczyna ! Podczas pracy z Eweliną: śmialiśmy się…baliśmy… i zachwycaliśmy. Budowa potwornie dała nam w kość, nie wiem tylko czy projektantka częściej była naszą morfiną czy katem :) Wiem na pewno, że to dzięki tej drobnej złożonej z kosmicznych pierwiastków istocie, powstał dom - pełny pasji i stylu, a my mieliśmy zaszczyt poznać kogoś przepełnionego do szpiku kości radością tworzenia i realizowania…',
        state: 'inactive'
      },
      {
        person: 'Ania, Warszawa',
        text: 'Współpraca z Eweliną od samego początku do zakończenia projektu była na bardzo wysokim poziomie. Ewelina w czasie realizacji projektu mojego pierwszego mieszkania poświęciła mi bardzo dużo czasu na omówienie różnych możliwości i zawsze elastycznie umiała podejść do moich oczekiwań. Moje mieszkanie wykonane zgodnie z projektem Eweliny jest cudownym miejscem, które zachwyca wielu gości. Gorąco polecam współpracę z Eweliną, gdyż oprócz profesjonalizmu, ogromnej kreatywności i jej dużego zaangażowania jest osobą o bardzo dużej empatii, serdeczności i ciepła. Jeśli będę urządzać swoje kolejne lokum z pewnością zwrócę się do niej o pomoc w tworzeniu kolejnego pięknego miejsca.',
        state: 'inactive'
      },
      {
        person: 'Magda i Błażej, Piotrków Tryb.',
        text: 'Pracowania EWENEMENT towarzyszyła nam przy projektowaniu głównych pomieszczeń w naszym domu i spełniła nasze oczekiwania w 100%. Pani Ewelina dołożyła wszelkich starań, abyśmy byli zadowoleni z efektu końcowego. W trakcie pracy nad projektem stale się z nami kontaktowała i ustalała najdrobniejsze szczegóły, pozostając przy tym czuła na wszystkie nasze uwagi i sugestie. Jesteśmy usatysfakcjonowani jakością wykonanych dla nas projektów wnętrz, które są w pełni funkcjonalne, ale przede wszystkim naprawdę piękne i z przyjemnością codziennie się w nich żyje. Polecamy współpracę z tą pracownią.',
        state: 'inactive'
      },
      {
        person: 'Danka i Szymon, Warszawa',
        text: 'Współpraca ze Studiem EWENEMENT to czysta przyjemniość. Pani Architekt sporządzała projekt starannie dostosowując go do naszych indywidualnych potrzeb. Opowiedzieliśmy jej z mężem o naszych wymaganiach, co nam się podoba, a co nie, wszystko to zostało uwzględnione. Doradziła, które z naszych pomysłów mogą być niepraktyczne i na co warto zwrócić uwagę. Jednocześnie sama zaproponowała wiele rozwiązań, które bardzo dobrze się sprawdziły. Dodatkowo Pani Ewelina jest niezwykle serdeczną osobą. Gorąco polecamy korzystanie z jej usług!',
        state: 'inactive'
      },
      {
        person: 'Anna, Piotrków Tryb.',
        text: 'Korzystałam z usług Studia Projektowania Wnętrz EWENEMENT. Projekt został wykonany terminowo z należytą starannością, z właściwą szczegółowością i trafnością przyjętych rozwiązań. Współpraca przebiegała w miłej i życzliwej atmosferze. Dostepność mailowa i telefoniczna o każdej porze dnia, zawsze mogłam skorzystać z dodatkowych informacji lub dokonać drobnych korekt. Z czystym sumieniem polecam panią architekt Ewelinę celem spełnienia swoich marzeń o przepięknym mieszkaniu.',
        state: 'inactive'
      },
      {
        person: 'Danna, Warszawa',
        text: 'Bardzo polecam Ewelinę do pomocy w projektowaniu i urządzaniu wnętrz. Ma świetne wyczucie gustu klienta, jest bardzo elastyczna jesli chodzi o zmiany w projekcie. Jest pomocna w najmniejszych detalach oraz zawsze służy radą jesli chodzi o dobór materiałów. Dba o kazdy szczegół, a efektem jej pracy jest zadowolony klient :)',
        state: 'inactive'
      }
    ]
  };

  getData(URL) {
    for (let key in this.emittedData) {
      if (this.emittedData.hasOwnProperty(key)) {
        if (URL === key) {
          switch (URL) {
            case 'stage':
              this.stageObserver.next(this.emittedData[key]);
              break;
            case 'objective':
              this.objectiveObserver.next(this.emittedData[key]);
              break;
            case 'offer':
              this.offerObserver.next(this.emittedData[key]);
              break;
            case 'numbers':
              this.numbersObserver.next(this.emittedData[key]);
              break;
            case 'documentation':
              this.documentationObserver.next(this.emittedData[key]);
              break;
            case 'reason':
              this.reasonObserver.next(this.emittedData[key]);
              break;
            case 'doYouKnow':
              this.doYouKnowObserver.next(this.emittedData[key]);
              break;
            case 'aboutMe':
              this.aboutMeObserver.next(this.emittedData[key]);
              break;
            case 'contactDetails':
              this.contactDetailsObserver.next(this.emittedData[key]);
              break;
            case 'references':
              this.referencesObserver.next(this.emittedData[key]);
              break;
          }
        }
      }
    }
  }


  stageObserver: Observer<object>;
  stageEmitter: Observable<object> = new Observable(data => this.stageObserver = data);
  objectiveObserver: Observer<object>;
  objectiveEmitter: Observable<object> = new Observable(data => this.objectiveObserver = data);
  offerObserver: Observer<object>;
  offerEmitter: Observable<object> = new Observable(data => this.offerObserver = data);
  numbersObserver: Observer<object>;
  numbersEmitter: Observable<object> = new Observable(data => this.numbersObserver = data);
  documentationObserver: Observer<object>;
  documentationEmitter: Observable<object> = new Observable(data => this.documentationObserver = data);
  reasonObserver: Observer<object>;
  reasonEmitter: Observable<object> = new Observable(data => this.reasonObserver = data);
  doYouKnowObserver: Observer<object>;
  doYouKnowEmitter: Observable<object> = new Observable(data => this.doYouKnowObserver = data);
  aboutMeObserver: Observer<object>;
  aboutMeEmitter: Observable<object> = new Observable(data => this.aboutMeObserver = data);
  contactDetailsObserver: Observer<object>;
  contactDetailsEmitter: Observable<object> = new Observable(data => this.contactDetailsObserver = data);
  referencesObserver: Observer<object>;
  referencesEmitter: Observable<object> = new Observable(data => this.referencesObserver = data);


}
