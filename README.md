Każdy component który będzie miał treść ze zmienialnym językiem powinien mieć subscribe na languageObserver z language.service.

Na ten moment jest to zbudowane w ten sposób, że servis odpowiadający za dany component ma (na przykładzie menu):
 
  listen() {
    this.languageService.languageEmitter.subscribe((data: Language) => {
      this.menuObserver.next(data.menu);
    });
  }
  
  czyli bierze sobie odpowiednią część danych języka.
  
  A sam component:
  
      this.menuService.menuObservable.subscribe((menu: MenuItem[]) => {
        this.sections = menu.filter((element) => element.type === 'section');
        this.menu = menu;
        this.iconSrc = this.menu.filter((element) => element.type === 'icon')[0].iconURL;
        this.languageButton = this.menu.filter((element) => element.type === 'language')[0].language;
      });
      
   Czyli już sobie sam operuje na przychodzących danych.
   
   język menu ma formę:
     "menu": [
         {
           "type": "section",
           "title": "PO CO?",
           "path": "why"
         },
         {
           "type": "section",
           "title": "O MNIE",
           "path": "about"
         },
         {
           "type": "section",
           "title": "OFERTA",
           "path": "offer"
         },
         {
           "type": "section",
           "title": "REFERENCJE",
           "path": "refs"
         },
         {
           "type": "section",
           "title": "PROJEKTY",
           "path": "projects"
         },
         {
           "type": "icon",
           "path": "contact",
           "iconURL": "./assets/mail.png"
         },
         {
           "type": "language",
           "language": "EN"
         }
       ]
   
   Więc typowy artykuł będzie miał coś w rodzaju:
   [
   {
   "type": "title",
   "content": "Tytuł artykułu", 
   },   
   {
   "type": "subtitle",
   "content": "Podtytuł artykułu", 
   },
   {
   "type": "article",
   "content": "Treść artykułu"
   }
   ]
   
